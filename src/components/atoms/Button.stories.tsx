import { Meta, StoryFn } from '@storybook/react';
import Button from '@/components/atoms/Button';

const meta: Meta<typeof Button> = {
  component: Button,
  tags: ['autodocs'],
  args:{
  },
  argTypes: {
  },
};
export default meta;

export const Default: StoryFn = (args) => {
  return(
      <Button {...args}></Button>

  );
};


