// app/components/molecules/ButtonGroup.tsx
import React from 'react';

interface ButtonGroupProps {
  className?: string;
}

const ButtonGroup: React.FC<ButtonGroupProps> = ({
  className = '',
}) => {
  return (
    <div>
      <button>a</button>
      <button>aa</button>
    </div>
  );
};

export default ButtonGroup;

