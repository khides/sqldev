import { Meta, StoryFn } from '@storybook/react';
import ButtonGroup from '@/components/molecules/ButtonGroup';

const meta: Meta<typeof ButtonGroup> = {
  component: ButtonGroup,
  tags: ['autodocs'],
  args:{
  },
  argTypes: {
  },
};
export default meta;

export const Default: StoryFn = (args) => {
  return(
    <ButtonGroup {...args}></ButtonGroup>
  );
};


