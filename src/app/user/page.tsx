'use client';

import React from 'react';
import { useState } from 'react';

export default function UserPage() {
  const [userId, setUserId] = useState<number | null>(null);
  const [userData, setUserData] = useState<any>(null);
  const [error, setError] = useState<string | null>(null);

  const fetchUserData = async (id: number) => {
    try {
      const response = await fetch(`/api/user/${id}`);
      console.log(response);
      
      if (!response.ok) {
        throw new Error('Failed to fetch user data');
      }
      const data = await response.json();
      setUserData(data);
      setError(null);
    } catch (error) {
      setError('Error fetching user data');
      console.error(error);
      
      setUserData(null);
    }
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (userId !== null) {
      fetchUserData(userId);
    }
  };

  return (
    <div>
      <h1>Find User by ID</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="number"
          value={userId ?? ''}
          onChange={(e) => setUserId(Number(e.target.value))}
          placeholder="Enter User ID"
          required
          className="text-black border border-gray-300 p-2 rounded"
        />
        <button type="submit">Fetch User</button>
      </form>

      {error && <p style={{ color: 'red' }}>{error}</p>}

      {userData && (
        <div>
          <h2>User Data:</h2>
          <p>ID: {userData.user?.id}</p>
          <p>Name: {userData.user?.name}</p>
          <p>Is Admin: {userData.user?.is_admin ? 'Yes' : 'No'}</p>
        </div>
      )}
    </div>
  );
}
