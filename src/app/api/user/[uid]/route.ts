// import { NextApiRequest, NextApiResponse } from 'next';
// import { loadPackageDefinition, credentials } from '@grpc/grpc-js';
// import { loadSync } from '@grpc/proto-loader';
// import path from 'path';
import {UserManagerClient} from '../../../../../protos/__generated__/user_grpc_pb'
import {UserRequest,UserResponse, User} from '../../../../../protos/__generated__/user_pb.ts'
import * as grpc from '@grpc/grpc-js';
import { NextRequest, NextResponse } from 'next/server';


// ユーザーのモックデータ
const mockUsers = [
  { id: 1, name: 'Alice', isAdmin: true },
  { id: 2, name: 'Bob', isAdmin: false },
  { id: 3, name: 'Charlie', isAdmin: true },
  { id: 4, name: 'David', isAdmin: false },
  { id: 5, name: 'Eve', isAdmin: true },
];

// UserManagerClientのモック実装
export class MockUserManagerClient  {
  constructor(private address: string, private credentials: grpc.ChannelCredentials, private options?: object) {}

  // findByIdメソッドのモック
  public findById(
    request: UserRequest,
    callback: (error: grpc.ServiceError | null, response: UserResponse) => void
  ): grpc.ClientUnaryCall {
    const userId = request.getId();

    // ユーザーのIDに基づいてランダムなデータを検索
    const user = mockUsers.find((u) => u.id === userId);

    if (!user) {
      // ユーザーが見つからなければエラーを返す
      const error: grpc.ServiceError = {
        name: 'NotFoundError',
        message: 'User not found',
        code: grpc.status.NOT_FOUND,
        details: 'No user with the given ID exists',
        metadata: new grpc.Metadata(),
      };
      callback(error, null as any);
    } else {
      // UserResponseを作成して返す
      const response = new UserResponse();
      const userMessage = new User();
      userMessage.setId(user.id);
      userMessage.setName(user.name);
      userMessage.setIsAdmin(user.isAdmin);

      response.setUser(userMessage);

      // 正常なレスポンスをコールバックで返す
      callback(null, response);
    }

    // ダミーのgrpc.ClientUnaryCallを返す
    return {} as grpc.ClientUnaryCall;
  }
}

// // Protoファイルのパス
// const PROTO_PATH = path.resolve('./protos/externals/user.proto');

// // gRPC クライアントの設定
// const packageDefinition = loadSync(PROTO_PATH, {
//   keepCase: true,
//   longs: String,
//   enums: String,
//   defaults: true,
//   oneofs: true,
// });
// const grpcObject = loadPackageDefinition(packageDefinition);
// const UserManager = grpcObject.UserManager as any;

// // gRPCクライアントインスタンスの作成
// const client = new UserManager(
//   'localhost:50051',
//   credentials.createInsecure()
// );

// 名前付きエクスポートで GET リクエストを処理
export async function GET(req: NextRequest) {
  // const { searchParams } = new URL(req.url);
  // const id = searchParams.get('uid');
   // URLのパスからuidを取得
   const { pathname } = req.nextUrl;
   const id = pathname.split('/').pop(); // 最後のパスセグメントが uid
 
  // const client = new UserManagerClient(
  //   "localhost:50051",
  //   credentials.createInsecure()
  // );
  const client = new MockUserManagerClient('', grpc.credentials.createInsecure());
  console.log('id', id);
  

  if (!id) {
    return NextResponse.json({ error: 'ID is required' }, { status: 400 });
  }

  const request = new UserRequest();
  request.setId(Number(id));

  return new Promise((resolve) => {
    client.findById(request, (error, response) => {
      if (error) {
        resolve(
          NextResponse.json({ error: 'gRPC call failed', details: error.message }, { status: 500 })
        );
      } else {
        resolve(NextResponse.json(response.toObject(), { status: 200 }));
      }
    });
  });
}


