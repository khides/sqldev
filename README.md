### 環境構築

VSCodeでレポジトリをcloneし,

1. ファイル作成
   - `./.env.example`->`.env`
2. コンテナ立ち上げ
   - `cmd + Shift + P`から`Dev Containers: Reopen in Container`
3. それぞれVSCodeの`Run and Debug`から実行

#### 注意
必要なコンテナ以外落とす
```bash
docker compose down
```


// "codegen": "grpc_tools_node_protoc --ts_out=import_style=es6,binary:./protos/__generated__ --grpc_out=grpc_js:./protos/__generated__ --proto_path=./protos/externals ./protos/externals/user.proto"

grpc_tools_node_protoc \
  --plugin=protoc-gen-ts=./node_modules/.bin/protoc-gen-ts \
  --ts_out=grpc_js:./protos/__generated__ \
  --grpc_out=grpc_js:./protos/__generated__ \
  --proto_path=./protos/externals ./protos/externals/user.proto

test