#!/bin/bash
# cmp.sh
# description : Create a component and a story with snippets

# Check if the first argument is empty
if [ -z $2 ]; then
  echo "Please enter the component name."
  exit 1
fi
# Check if the second argument is empty
if [ -z $1 ]; then
  echo "Please enter the component type."
  exit 1
fi

# Create a component
case $1 in
  -a | --atom)
    # Create directories if they do not exist
    mkdir -p ../src/components/atoms
    
    # Check if the component is already exists
    if [ -e ../src/components/atoms/$2.tsx ]; then
      echo "The component already exists."
      # Check if the storybook already exists
      if [ -e ../src/components/atoms/$2.stories.tsx ]; then
        echo "The storybook already exists."
        exit 1
      else
        touch ../src/components/atoms/$2.stories.tsx
        source ./snp/str/atmsnp.sh $2
        code ../src/components/atoms/$2.stories.tsx
      fi
    else
      touch ../src/components/atoms/$2.tsx
      source ./snp/cmp/atmsnp.sh $2
      touch ../src/components/atoms/$2.stories.tsx
      source ./snp/str/atmsnp.sh $2
      code ../src/components/atoms/$2.tsx
    fi
    ;;

  -m | --molecule)
    # Create directories if they do not exist
    mkdir -p ../src/components/molecules
    
    # Check if the component is already exists
    if [ -e ../src/components/molecules/$2.tsx ]; then
      echo "The component already exists."
      # Check if the storybook already exists
      if [ -e ../src/components/molecules/$2.stories.tsx ]; then
        echo "The storybook already exists."
        exit 1
      else
        touch ../src/components/molecules/$2.stories.tsx
        source ./snp/str/mlcsnp.sh $2
        code ../src/components/molecules/$2.stories.tsx
      fi
    else
      touch ../src/components/molecules/$2.tsx
      source ./snp/cmp/mlcsnp.sh $2
      touch ../src/components/molecules/$2.stories.tsx
      source ./snp/str/mlcsnp.sh $2
      code ../src/components/molecules/$2.tsx
    fi
    ;;

  -o | --organism)
    # Create directories if they do not exist
    mkdir -p ../src/components/organisms
    
    # Check if the component is already exists
    if [ -e ../src/components/organisms/$2.tsx ]; then
      echo "The component already exists."
      # Check if the storybook already exists
      if [ -e ../src/components/organisms/$2.stories.tsx ]; then
        echo "The storybook already exists."
        exit 1
      else
        touch ../src/components/organisms/$2.stories.tsx
        source ./snp/str/orgsnp.sh $2
        code ../src/components/organisms/$2.stories.tsx
      fi
    else
      touch ../src/components/organisms/$2.tsx
      source ./snp/cmp/orgsnp.sh $2
      touch ../src/components/organisms/$2.stories.tsx
      source ./snp/str/orgsnp.sh $2
      code ../src/components/organisms/$2.tsx
    fi
    ;;

  -t | --template)
    # Create directories if they do not exist
    mkdir -p ../src/components/templates
    
    # Check if the component is already exists
    if [ -e ../src/components/templates/$2.tsx ]; then
      echo "The component already exists."
      # Check if the storybook already exists
      if [ -e ../src/components/templates/$2.stories.tsx ]; then
        echo "The storybook already exists."
        exit 1
      else
        touch ../src/components/templates/$2.stories.tsx
        source ./snp/str/tmpsnp.sh $2
        code ../src/components/templates/$2.stories.tsx
      fi
    else
      touch ../src/components/templates/$2.tsx
      source ./snp/cmp/tmpsnp.sh $2
      touch ../src/components/templates/$2.stories.tsx
      source ./snp/str/tmpsnp.sh $2
      code ../src/components/templates/$2.tsx
    fi
    ;;

  *)
    echo "Invalid component type."
    exit 1
    ;;

esac
