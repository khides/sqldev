#!/bin/bash
# atmsnp.sh
# description : Atom's snippet
echo "import { Meta, StoryFn } from '@storybook/react';
import $1 from '@/components/atoms/$1';

const meta: Meta<typeof $1> = {
  component: $1,
  tags: ['autodocs'],
  args:{
  },
  argTypes: {
  },
};
export default meta;

export const Default: StoryFn = (args) => {
  return(
      <$1 {...args}></$1>

  );
};

" > ../src/components/atoms/$1.stories.tsx
