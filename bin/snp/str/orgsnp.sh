#!/bin/bash
# orgsnp.sh
# description : Organism's snippet
echo "import { Meta, StoryFn } from '@storybook/react';
import $1 from '@/components/organisms/$1';

const meta: Meta<typeof $1> = {
  component: $1,
  tags: ['autodocs'],
  args:{
  },
  argTypes: {
  },
};
export default meta;

export const Default: StoryFn = (args) => {
  return(
    <$1 {...args}></$1>
  );
};

" > ../src/components/organisms/$1.stories.tsx
