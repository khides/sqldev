#!/bin/bash
# orgsnp.sh
# description : Atom's snippet
echo "// app/components/organisms/$1.tsx
import React from 'react';

interface ${1}Props {
  className?: string;
}

const $1: React.FC<${1}Props> = ({
  className = '',
}) => {
  return(
  );
};

export default $1;
" > ../src/components/organisms/$1.tsx
