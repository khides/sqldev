#!/bin/bash
# atmsnp.sh
# description : Atom's snippet
echo "// app/components/atoms/$1.tsx
import React from 'react';

interface ${1}Props {
  className?: string;
}

const $1: React.FC<${1}Props> = ({
  className = '',
}) => {
  return (
  );
};

export default $1;
" > ../src/components/atoms/$1.tsx
