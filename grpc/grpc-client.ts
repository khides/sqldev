// src/grpc/grpc-client.ts
import { createPromiseClient } from '@bufbuild/connect';
import { UserManager } from '../protos/__generated__/user_pb'; // 生成されたクライアント
import { createConnectTransport } from '@bufbuild/connect-web'; // 正しいインポート

// gRPC サーバーのエンドポイントを指定
const transport = createConnectTransport({
  baseUrl: 'https://your-grpc-server.com', // gRPC サーバーのURL
});

// gRPC クライアントを作成
export const UserClient = createPromiseClient(UserManager, transport);
